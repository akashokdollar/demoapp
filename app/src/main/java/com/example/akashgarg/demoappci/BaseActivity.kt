package com.example.akashgarg.demoappci

import android.support.v7.app.AppCompatActivity
import android.widget.Toast

open class BaseActivity : AppCompatActivity() {

    fun showToast() {
        Toast.makeText(this@BaseActivity, "Hi, I am Hello World ! ", Toast.LENGTH_LONG).show()
    }
}